import { Injectable } from "@angular/core";
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse,
} from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError, tap, finalize, map } from "rxjs/operators";

@Injectable()
export class InterceptorLanguage implements HttpInterceptor {
  constructor() {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{
   const lang = localStorage.getItem('lang') || 'en';

   request = request.clone({
     setHeaders: {
       'Accept-Language': lang
     }
   })

    return next.handle(request);
  }


}
