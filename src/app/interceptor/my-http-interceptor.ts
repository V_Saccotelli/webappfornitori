import { Injectable } from "@angular/core";
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse,
} from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError, tap, finalize, map } from "rxjs/operators";
import { Router } from "@angular/router";
import { GlobalService } from "app/shared/global.service";

@Injectable()
export class MyHttpInterceptor implements HttpInterceptor {
  constructor(
    private router: Router,
    private global : GlobalService
  ) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    request = request.clone({
      setHeaders: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    });
    return next.handle(request).pipe(
      tap((_) => {
        if (_ instanceof HttpResponse) {
       
          return _;
        }
      }),
      catchError((error: HttpErrorResponse) => {
        if (error.status === 401) {
         // sessionStorage.removeItem('HechRomeoToken');
         // this.router.navigate(['/login']);

         this.global.showError("La sessione è scaduta, rieffettua il login")
         this.logout()
        }
    
        return throwError(error);
      })
    );
  }

  logout() {
    sessionStorage.clear()
    localStorage.clear()
    this.router.navigate(['/homepage'])
  }
}
