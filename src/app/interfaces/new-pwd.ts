export interface NewPwd {
  email: string;
  oldpwd: string;
  newpwd: string;
}
