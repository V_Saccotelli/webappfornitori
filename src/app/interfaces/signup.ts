export interface Signup {
    providergeneral:
    {
        companyname: string,
        legalstatusid: string,
        businessareaid: string,
		    sectorid: string,
        categoryid: string,
        IDI: string,
        IBAN: string
    },
    provideraddress:
    {
        address: string,
        NAP: number,
        city: string,
        district: string
    },
    providerreferrer:
    {
        name: string,
        lastname: string,
        email: string,
        department: string,
        phone: number,
        mobile: number
    }

}
