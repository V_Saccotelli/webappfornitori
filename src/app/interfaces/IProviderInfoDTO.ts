export default interface IProviderInfoDTO{
    
    providerid: string,
    general: {
        companyname: string,
        legalstatusid: number,
        businessareaid: number,
        sectorid: number,
        categoryid: number,
        idi:string,
        iban:string
    },
    addresses: [
        {
            addresstype: number,
            address: string,
            nap: string,
            city: string,
            district: string
        }
    ],
    referrers: [
        {
            name: string,
            lastname: string,
            email: string,
            department:string,
            phone: string,
            mobile: string
        }
    ]
}