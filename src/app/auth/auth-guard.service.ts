import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from './auth.service';
import { GlobalService } from '../shared/global.service';
@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(public auth: AuthService, public router: Router,private global:GlobalService) {}
  canActivate(): boolean {
    // if (!this.auth.isAuthenticated()) {
    //   this.global.showError('Non sei loggato!')
    //   this.router.navigate(['login']);
    //   return false;
    // }
    return true;
  }
}