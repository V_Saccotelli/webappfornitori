import { Injectable, OnInit } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { HttpClient, HttpHeaders, HttpRequest } from "@angular/common/http";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class GlobalService implements OnInit{
  errorit1 = 'Ops..si è verificato un errore';
  erroren1 = 'Ops..there is an error';

  successit1 = 'Registrazione avvenuta con successo! Riceverai una mail con le prossime istruzioni';
  successen1 = 'Registration was successful! You will receive an email with instructions';

  successLoginit = 'Login effettuato con successo';
  successLoginen = 'Login successful';

  addServiceErrorit = 'Inserisci tutti i campi';
  addServiceErroren = 'Enter all input fields';

  addServiceSuccessit = 'Servizio aggiunto con successo';
  addServiceSuccessen = 'Service added successfully';

  modServiceErrorit = 'Servizio modificato con successo';
  modServiceErroren = 'Service changed successfully';

  modpwdSuccit = 'Password modificata correttamente';
  modpwdSuccen = 'Password changed successfully';


  constructor(private toastr: ToastrService,
    private http: HttpClient,
    ) {}

  ngOnInit() {

  }
  getHeader() {
    //var token:any;
    var headers;
    var lang = localStorage.getItem('lang');
    //token = sessionStorage.getItem('token');

    headers = new HttpHeaders({
      //'Authorization': 'Bearer ' + token,
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*"
    });
    return headers;
  }

  showSuccess(message) {
    this.toastr.success(message);
  }
  showError(message) {
    this.toastr.error(message);
  }

  getImage(id) {
    var url = environment.apiUrl + "api/media/get/image/" + id;
    return this.http.get(url, { headers: this.getHeader() });
  }
}
