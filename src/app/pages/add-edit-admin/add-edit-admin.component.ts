import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../shared/global.service';
import { Router } from '@angular/router';
import * as $ from 'jquery';

declare interface TableData {
    headerRow: string[];
    dataRows: string[][];
}

@Component({
    selector: 'add-edit-admin-cmp',
    moduleId: module.id,
    templateUrl: 'add-edit-admin.component.html'
})

export class AddEditAdminComponent implements OnInit {

    admin: any;

    mynickname:any;

    constructor(
        private global: GlobalService,
        private router: Router,
        ) {

        this.admin = this.router.getCurrentNavigation().extras.queryParams.admin
   


    }

    ngOnInit() {

        this.mynickname = sessionStorage.getItem('nickname')

        if (this.admin != 'new') {

         
            $("#name").val(this.admin.name);
            $("#lastname").val(this.admin.lastname);
            $("#email").val(this.admin.email);
 

        }

    }

    addNewAdmin() {

        // function isAlphaNum(s) { // this function tests it
        //     var p = /([0-9].*[a-zA-Z])|([a-zA-Z].*[0-9])/;
        //     return p.test(s);
        // }

        // var nickname = $("#nickname").val();
        // var nome = $("#name").val();
        // var lastname = $("#lastname").val();
        // var email = $("#email").val();
        // var password = $("#password").val();


        // if (nickname && nome && lastname && email && password) {

        //     if (password.toString().length < 8) {
        //         this.global.showError('La password deve contenere almeno 8 caratteri')
        //     } else if (isAlphaNum(password.toString()) === false) {
        //         this.global.showError('La password deve contenere almeno una maiuscola e un numero')
        //     } else {
        //         this.usersService.addAdmin(nickname, nome, lastname, email, password).subscribe(ok => {
        //             this.global.showSuccess('Admin creato correttamente!')
        //         }, err => {
        //             this.global.showError('Ops! Si è verificato un errore')
        //         })
        //     }

        // } else {
        //     this.global.showError('Tutti i campi sono obbligatori');
        // }

    }

    editAdmin() {


        
        // var nome = $("#name").val();
        // var lastname = $("#lastname").val();
        // var email = $("#email").val();

       

        // if (this.admin.nickname && nome && lastname && email && this.admin.password) {


        //     this.usersService.updateAdmin(this.admin.nickname, nome, lastname, email, this.admin.password).subscribe(ok => {
        //         this.global.showSuccess('Admin modificato correttamente!')
        //     }, err => {
        //         this.global.showError('Ops! Si è verificato un errore')
        //     })


        // } else {
        //     this.global.showError('Tutti i campi sono obbligatori');
        // }

    }

    resetPsw(){
      
        // this.usersService.resetPassword(this.admin.nickname).subscribe(ok=>{
           
        //     this.global.showSuccess('Password resettata correttamente!');
        //     var defaultpsw:any = ok
        //     defaultpsw = defaultpsw.password
        //     $("#defaultpass").html('Inserisci questa password nella schermata di login per avviare la procedura di modifica password:   ' + defaultpsw);
            
        // }, err => {
        //     this.global.showError('Ops! Si è verificato un errore')
        // })
        
    }

    
  changePassword() {

    // var account = $("#accounttomod").val()
    // var oldpassword = $("#oldpassword").val()
    // var newpassword = $("#newpassword").val()



    // function isAlphaNum(s) { // this function tests it
    //   var p = /([0-9].*[a-zA-Z])|([a-zA-Z].*[0-9])/;
    //   return p.test(s);
    // }

    // if(account && oldpassword && newpassword){
    //   if (newpassword.toString().length < 8) {
    //     this.global.showError('La password deve contenere almeno 8 caratteri')
    //   } else if (isAlphaNum(newpassword.toString()) === false) {
    //     this.global.showError('La password deve contenere almeno una maiuscola e un numero')
    //   } else {
    //     this.userService.changePassword(account, oldpassword, newpassword).subscribe(ok => {
          
    //       this.global.showSuccess('Password modificata correttamente!')

  
    //     }, err => {
     
    //       this.global.showError('Ops! Si è verificato un errore')
    //     })
    //   }
    // }else{
    //   this.global.showError('Devi compilare tutti i campi!')
    // }




  }
}
