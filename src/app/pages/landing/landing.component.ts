import { GlobalService } from 'app/shared/global.service';
import { LoginService } from "./../../services/login.service";
import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
@Component({
  selector: "app-landing",
  templateUrl: "./landing.component.html",
  styleUrls: ["./landing.component.scss"],
})
export class LandingComponent implements OnInit {
  email: string =  'vincenzo.saccotelli@nextre.it';
  password: string = '1234Password0987';
  constructor(
    private loginService: LoginService,
    private router: Router,
    translate: TranslateService,
    private global: GlobalService
  ) {
    var language = window.navigator.language;
    language = language.substring(0, 2);


    translate.setDefaultLang(language)
    localStorage.setItem('lang', language);

    if (language === "it") {
      translate.setDefaultLang("italiano");
    } else if (language === "en") {
      translate.setDefaultLang("inglese");
    } else {
      translate.setDefaultLang("inglese");
    }
  }

  ngOnInit(): void {}
  showPassword() {
    var x: any = document.getElementById("password");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }

  login() {
       if (this.email && this.password) {
         this.loginService.login(this.email, this.password).subscribe(resp => {
           //console.log('loginreso',resp);
           let data: any = resp;

           localStorage.setItem('providerID', data.companyid);
           localStorage.setItem('accountID', data.accountid);
           localStorage.setItem('token', data.token);
           localStorage.setItem('email', this.email);
           localStorage.setItem('companyID', data.companyid);
           this.router.navigate(["/convenzioni"]);

           let language = window.navigator.language;
           language = language.substring(0, 2);
           if (language === 'it') {
            this.global.showSuccess(this.global.successLoginit)
          }else if(language === 'en'){
            this.global.showSuccess(this.global.successLoginen)
          }
         }, error => {
           console.log('error: ', error)
           if (error.status === 535) {
               this.router.navigate(["changepwd"]);
           }
           let language = window.navigator.language;
           language = language.substring(0, 2);
           if(language === 'it'){
             this.global.showError(this.global.errorit1)
           }else if(language === 'en'){
             this.global.showError(this.global.erroren1)
           }
         })
       }
  }
}
