import { EmitService } from './../../services/emit.service';
import { TranslateService } from '@ngx-translate/core';
import { HttpHeaders } from '@angular/common/http';
import { ConvenzioniService } from './../../services/convenzioni.service';
import { Component, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { GlobalService } from '../../shared/global.service';
import { Router } from '@angular/router';
import * as $ from 'jquery';
declare interface TableData {
    headerRow: string[];
    dataRows: string[][];
}

@Component({
    selector: 'convenzioni-cmp',
    moduleId: module.id,
    templateUrl: 'convenzioni.component.html'
})

export class ConvenzioniComponent implements OnInit {

    convenzioni: any;
    status: any;
    lang:any;
    constructor(
        private translateService: TranslateService,
        private global: GlobalService,
        private evtEmitter: EmitService,
        private router: Router,
        private convService: ConvenzioniService) {

         }


    ngOnInit() {
      this.translateService.setDefaultLang('it');
      this.translateService.use(localStorage.getItem('lang'));
      this.lang = localStorage.getItem('lang');
      this.evtEmitter.entry.subscribe(resp => {
        this.ngOnInit();
      })

      this.getAgreementDetail()
    }
    getAgreementDetail(){
      this.convService.getAgreementDetail().subscribe(resp => {
        //console.log(resp);
        this.convenzioni = resp;
        this.status = this.convenzioni.status;
      })
    }

    disattivazione(){
      this.convService.unsubscribeAgreement().subscribe(resp => {
        console.log(resp);
      })
    }

    addeditConvenzione(d){
      this.router.navigate(['/convenzione', d]);
    }

 /*    addeditAdmin(d){
        this.router.navigate(['/admin'],{queryParams:{ admin: d }});
    }

    addeditAzienda(d){
        this.router.navigate(['/azienda'],{queryParams:{ azienda: d }});
    }

    addeditFornitore(d){
        this.router.navigate(['/fornitore'],{queryParams:{ fornitore: d }});
    }

    addeditConvenzione(d){
        this.router.navigate(['/convenzione'],{queryParams:{ convenzione: d }});
    } */

}
