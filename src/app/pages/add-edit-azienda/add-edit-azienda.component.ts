import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../shared/global.service';
import { Router } from '@angular/router';
import * as $ from 'jquery';


declare interface TableData {
    headerRow: string[];
    dataRows: string[][];
}

@Component({
    selector: 'add-edit-azienda-cmp',
    moduleId: module.id,
    templateUrl: 'add-edit-azienda.component.html'
})

export class AddEditAziendaComponent implements OnInit {

    azienda:any

    constructor(
        
        private global: GlobalService,
        private router: Router) {

      
            this.azienda = this.router.getCurrentNavigation().extras.queryParams.azienda
            console.log('azienda',this.router.getCurrentNavigation());

    }

    ngOnInit() {

      
    }


}
