import { GlobalService } from './../../shared/global.service';
import { MediaService } from './../../services/media.service';
import { Imediadto } from './../../interfaces/imediadto';
import { ServiziService } from './../../services/servizi.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-updateservice',
  templateUrl: './updateservice.component.html',
  styleUrls: ['./updateservice.component.css']
})
export class UpdateserviceComponent implements OnInit {
  file: any;
  loadedFile: any;
  loadedFileId: string;
  mediatype: any;
  loadedVideo:any;
  image:any;
  video:any;
  loadedPicture: any;
  loadedPictureId: string;
  loadedPicId: any;
  loadedVideoId: any;
  id: any;
  uploadedFile: any;
  uploadedFileId: string;
  typologies: any;
  services: any;

  service:any;
  price:any;
  allTypologies:any;
  constructor(private route: ActivatedRoute,
    private serviziService: ServiziService,
    private mediaService: MediaService,
    private globalService: GlobalService,
    private router: Router) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: any) => {
        //console.log(params);

         this.serviziService.getServiceDataById(params.service).subscribe(resp => {
           this.service = resp;
           console.log(this.service);
         })
    });

    this.serviziService.getAllTypologies()
    .subscribe(resp => {
      console.log(resp);
      this.allTypologies = resp;
    })
  }

  onDocChange(event) {

    var that = this;
    this.file = event.target.files;
    var reader = new FileReader();
    reader.readAsDataURL(this.file[0]);
    reader.onload = function () {
      var base64: any = reader.result;
      base64 = base64.substring(base64.lastIndexOf(",") + 1);
      var file: Imediadto = {
        mediaobject: base64,
      };
      that.addMediaDocument(file);
    };
    reader.onerror = function (error) {
      console.error(error)
    };
  }

  addMediaDocument(file: Imediadto){

    this.mediaService.addMediaDocumentOrFile(file).subscribe((fileResult: any) =>{
      this.loadedFile = file.mediaobject
      this.loadedFileId = fileResult.id

      console.log('loadedFile',this.loadedFile);
      console.log('loadedFileId',this.loadedFileId);
    },err => {
      this.globalService.showError("Il file è troppo grande"+err);
  })
  }


  onMediaChange(event){

    this.mediatype = event
    console.log(this.mediatype, "mediatype")

    }

    onImageChange(event) {
      var that = this;

      this.image = event.target.files;

      // console.log("image", this.image);

      var reader = new FileReader();
      reader.readAsDataURL(this.image[0]);

      reader.onload = function () {
        var base64: any = reader.result;
        base64 = base64.substring(base64.lastIndexOf(",") + 1);

        var pic: Imediadto = {
          mediaobject: base64,
        };
        that.addMediaPicture(pic);
      };
      reader.onerror = function (error) {};
    }

    onVideoChange(event) {
      var that = this;

      this.video = event.target.files;


      var reader = new FileReader();
      reader.readAsDataURL(this.video[0]);

      reader.onload = function () {
        var base64: any = reader.result;
        base64 = base64.substring(base64.lastIndexOf(",") + 1);

        var video: Imediadto = {
          mediaobject: base64,
        };
        that.addMediaMovie(video);
      };
      reader.onerror = function (error) {};
    }


addMediaPicture(pic: Imediadto){
  this.mediaService.addMediaPicture(pic).subscribe((picResult : any) => {
    // console.log("pic id", picResult)
    this.loadedPicture = pic.mediaobject
    this.loadedPicId = picResult.id
    // console.log("picture", this.loadedPicture)
  }, err => {
    this.globalService.showError("Il file è troppo grande")
}

  )
}

addMediaMovie(video: Imediadto){
  this.mediaService.addMediaMovie(video).subscribe((videoResult: any) =>{
     console.log(" video id", videoResult)
    this.loadedVideo = video.mediaobject
    this.loadedVideoId = videoResult.id
    console.log("video", this.loadedVideo)
  }, err => {
    this.globalService.showError("Il file è troppo grande")
})
}



  updateService(){
    let updatedService: any = {
      serviceid: this.service.serviceid,
      providerid: localStorage.getItem('providerID'),
      typologyid: this.service.typologyid,
      price: this.service.price,
      discountedprice: this.service.discountedprice,
      discountaverage: this.service.discountaverage,
      enablemedia: true,
      mediaid: "",
      mediatype: this.service.mediatype,
      documentid: "",
      requireIndetifier: false,
      languages: [
          {
              language: "IT",
              title: this.service.languages[0].title,
              description: this.service.languages[0].description
          },
         {
              language: "EN",
              title: this.service.languages[1].title,
              description: this.service.languages[1].description
          }
      ]
  }

    this.serviziService.updateService(updatedService)
    .subscribe(resp => {
      console.log(resp);
      this.router.navigate(['categorieservizi']);
    })
  }

}
