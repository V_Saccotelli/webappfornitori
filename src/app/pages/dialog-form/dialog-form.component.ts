import { TranslateService } from '@ngx-translate/core';
import { EmitService } from './../../services/emit.service';
import { async, waitForAsync } from '@angular/core/testing';
import { Imediadto } from './../../interfaces/imediadto';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SectionService } from './../../services/section.service';
import { GlobalService } from './../../shared/global.service';
import { ServiziService } from './../../services/servizi.service';
import { MediaService } from './../../services/media.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, Inject, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-dialog-form',
  templateUrl: './dialog-form.component.html',
  styleUrls: ['./dialog-form.component.css']
})
export class DialogFormComponent implements OnInit {


  service: any = { languages: [{}] };
  closeResult = '';
  file: any;
  loadedFile: any;
  loadedFileId: string;
  mediatype: any = 1;
  loadedVideo: any;
  image: any;
  video: any;
  loadedPicture: any;
  loadedPictureId: string;
  loadedPicId: any;
  loadedVideoId: any;
  id: any;
  uploadedFile: any;
  uploadedFileId: string;
  typologies: any;
  services: any;

  allTypologies: any;

  categorie: any = []
  servizi: any = []

  titleIT!: string;
  descrIT!: string;
  titleEN!: string;
  descrEN!: string;
  sezione!: string;
  prezzo!: number;
  sconto!: number;
  importo_sconto!: number;
  typologyid!: any;
  enablemedia: any;
  sconto_medio: any;

  loadedPostPicture: any;

  public dataEditService: any;


  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private modalService: NgbModal,
    private translateService: TranslateService,
    private mediaService: MediaService,
    private serviziService: ServiziService,
    private global: GlobalService,
    private sectionService: SectionService,
    private route: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog,
    private evetEm: EmitService,
    private globalService: GlobalService,
  ) { }

  ngOnInit(): void {
    this.translateService.setDefaultLang('it');
    this.translateService.use(localStorage.getItem('lang'));
    //this.lang = localStorage.getItem('lang');
    this.evetEm.entry.subscribe(resp => {
      this.ngOnInit();
    })
    console.log('questo è oggettto: ' + JSON.stringify(this.data));

    if (this.data != null) {
      this.serviceAndImageOrVideoById();
    }
    this.serviziService.getAllTypologies()
      .subscribe(resp => {
        console.log(resp);
        this.allTypologies = resp;
      })
  }

  serviceAndImageOrVideoById(){
    this.serviziService.getServiceDataById(this.data).subscribe(resp => {


      this.dataEditService = resp;

      console.log('this.dataEditService',resp);


      if(this.dataEditService.mediatype == 2 && this.dataEditService.mediaid){
        this.getVideoById(this.dataEditService.mediaid);
      }else if(this.dataEditService.mediatype == 1 && this.dataEditService.mediaid){
        this.getPictureById(this.dataEditService.mediaid);
      }
      if(this.dataEditService.documentid != '' && this.dataEditService.documentid != ' ' && this.dataEditService.documentid != null){
        this.getDocFileById(this.dataEditService.documentid)
      }

    });
  }

  dismiss() {
    this.dialog.closeAll();
  }

  getAllTypologiesByLanguage(language) {

    const call = this.sectionService.getAllTypologiesByLanguage(language).subscribe((data: any) => {
      this.typologies = data;
    })
    return call
  }


  getServiceDetails() {
    this.serviziService
      .getServiceDataById(this.id)
      .subscribe((data: any) => {
        console.log(data, "SERVIZIO SINGOLO")
        this.service = data
        this.mediatype = data.mediatype
        this.service.languageItTitle = this.getLanguages(this.service.languages, "IT")
        this.service.languageEnTitle = this.getLanguages(this.service.languages, "EN")
        this.service.languageItDesc = this.getLanguages(this.service.languages, "IT")
        this.service.languageEnDesc = this.getLanguages(this.service.languages, "EN")
        if (this.service.mediatype == 1 && this.service.mediaid != null) {
          this.getPictureById(this.service.mediaid)
        } else if (this.service.mediatype == 2 && this.service.mediaid != null) {
          this.getVideoById(this.service.mediaid)
        }


        if (this.service.documentid != "" && this.service.documentid != " ") {
          this.getDocFileById(this.service.documentid)
        }

      })
  }
  getLanguages(languages: any[], selectedLanguage = "IT") {
    return languages.find(
      (singleLanguage: any) => singleLanguage.language === selectedLanguage
    )?.title;

  }

  onDocChange(event) {
    var that = this;
    this.file = event.target.files;
    var reader = new FileReader();
    reader.readAsDataURL(this.file[0]);
    reader.onload = function () {
      var base64: any = reader.result;
      base64 = base64.substring(base64.lastIndexOf(",") + 1);
      var file: Imediadto = {
        mediaobject: base64,
      };
      that.addMediaDocument(file);
    };
    reader.onerror = function (error) {
      console.error(error)
    };
  }

  addMediaDocument(file: Imediadto) {
    this.mediaService.addMediaDocumentOrFile(file).subscribe((fileResult: any) => {
      this.loadedFile = file.mediaobject
      this.loadedFileId = fileResult.id

      console.log('loadedFile', this.loadedFile);
      console.log('loadedFileId', this.loadedFileId);
    }, err => {
      this.globalService.showError("Il file è troppo grande" + err);
    })
  }

  onImageChange(event) {
    var that = this;
    this.image = event.target.files;
    // console.log("image", this.image);
    var reader = new FileReader();
    reader.readAsDataURL(this.image[0]);
    reader.onload = function () {
      var base64: any = reader.result;
      base64 = base64.substring(base64.lastIndexOf(",") + 1);
      var pic: Imediadto = {
        mediaobject: base64,
      };
      that.addMediaPicture(pic);
    };
    reader.onerror = function (error) { };
  }

  onVideoChange(event) {
    var that = this;
    this.video = event.target.files;
    var reader = new FileReader();
    reader.readAsDataURL(this.video[0]);
    reader.onload = function () {
      var base64: any = reader.result;
      base64 = base64.substring(base64.lastIndexOf(",") + 1);
      var video: Imediadto = {
        mediaobject: base64,
      };
      that.addMediaMovie(video);
    };
    reader.onerror = function (error) { };
  }


  addMediaPicture(pic: Imediadto) {
    this.mediaService.addMediaPicture(pic).subscribe((picResult: any) => {
      // console.log("pic id", picResult)
      this.loadedPicture = pic.mediaobject
      this.loadedPicId = picResult.id

    }, err => {
      this.globalService.showError("Il file è troppo grande")
    }

    )
  }

  addMediaMovie(video: Imediadto) {
    this.mediaService.addMediaMovie(video).subscribe((videoResult: any) => {
      console.log(" video id", videoResult)
      this.loadedVideo = video.mediaobject
      this.loadedVideoId = videoResult.id
      console.log("video", this.loadedVideo)
    }, err => {
      this.globalService.showError("Il file è troppo grande")
    })
  }

  getPictureById(pictureid: string) {
    this.mediaService.getPictureFileById(pictureid).subscribe((pictureResult: any) => {

      this.loadedPicture = pictureResult.image
      this.loadedPictureId = pictureid


    })
  }

  getVideoById(videoid: string) {
    this.mediaService.getVideoFileById(videoid).subscribe((videoResult: any) => {
      console.log("[getVideoById]videoResult", videoResult)
      this.loadedVideo = videoResult.video
      this.loadedVideoId = videoid
    })
  }
  getDocFileById(fileid: string) {

    this.mediaService.getDocFileById(fileid).subscribe((fileResult: any) => {


      this.uploadedFile = fileResult.file
      this.uploadedFileId = fileid

    })
  }

  onMediaChange(event) {
    this.mediatype = event
    console.log(this.mediatype, "mediatype")
  }
  chooseTypology(evt) {
    console.log(evt);
  }


  creaServizio() {

    if (this.loadedPicId) {
      this.loadedVideoId = null
    }
    if (this.loadedVideoId) {
      this.loadedPicId = null
    }
    if (this.typologyid == null
      || this.prezzo == null
      || this.sconto == null
      || this.mediatype == null
      || this.titleIT == null
      || this.descrIT == null
      || this.titleEN == null
      || this.descrEN == null
    ) {
      let language = window.navigator.language;
      language = language.substring(0, 2);
      if(language === 'it'){
        this.global.showError(this.global.addServiceErrorit)
      }else if(language === 'en'){
        this.global.showError(this.global.addServiceErroren)
      }
      //alert('Inserisci tutti i campi')
    } else {
      let nuovoServizio = {
        providerid: localStorage.getItem('providerID'),
        typologyid: this.typologyid,
        price: this.prezzo,
        discountedprice: this.sconto,
        discountaverage: this.sconto_medio,
        enablemedia: this.enablemedia,
        mediaid: this.loadedPicId || this.loadedVideoId,
        mediatype: parseInt(this.mediatype),
        documentid: this.loadedFileId || " ",
        requireIndetifier: false,
        languages: [
          {
            language: "IT",
            title: this.titleIT,
            description: this.descrIT
          },
          {
            language: "EN",
            title: this.titleEN,
            description: this.descrEN
          }
        ]
      }
      console.log(nuovoServizio);
      this.serviziService.createService(nuovoServizio).subscribe(resp => {
        //console.log(resp);
        let language = window.navigator.language;
        language = language.substring(0, 2);
        if (language === 'it') {
          this.global.showSuccess(this.global.addServiceSuccessit)
        }else if(language === 'en'){
          this.global.showSuccess(this.global.addServiceSuccessen)
        }
        const dialogRef = this.dialog.closeAll();
        this.evetEm.entry.emit('ok');
      })
    }
  }

  modificaServizio() {


   // console.log('this.loadedPicId',this.loadedPicId);
    //console.log('this.loadedVideoId',this.loadedVideoId);


    var mediaid = null
    if (this.loadedPicId != undefined) {
      this.loadedVideoId = null
     mediaid = this.loadedPicId
     console.log('mediaid',mediaid);
    }
    if (this.loadedVideoId != undefined) {
      this.loadedPicId = null
      mediaid = this.loadedVideoId
      console.log('mediaid',mediaid);
    }



    if (this.dataEditService.typologyid == null
      || this.dataEditService.price == null
      || this.dataEditService.discountedprice == null
      || this.dataEditService.discountaverage == null
      || this.dataEditService.languages[0].title == null
      || this.dataEditService.languages[0].description == null
      || this.dataEditService.languages[1].title == null
      || this.dataEditService.languages[1].description == null
    ) {
      let language = window.navigator.language;
      language = language.substring(0, 2);
      if(language === 'it'){
        this.global.showError(this.global.addServiceErrorit)
      }else if(language === 'en'){
        this.global.showError(this.global.addServiceErroren)
      }
    } else {
      let nuovoServizio = {
        serviceid: this.data,
        providerid: localStorage.getItem('providerID'),
        typologyid: this.dataEditService.typologyid,
        price: this.dataEditService.price,
        discountedprice: this.dataEditService.discountedprice,
        discountaverage: this.dataEditService.discountaverage,
        enablemedia: this.dataEditService.enablemedia,
        mediaid: mediaid,
        mediatype: parseInt(this.dataEditService.mediatype),
        documentid: this.dataEditService.loadedFileId,
        requireIndetifier: false,
        languages: [
          {
            language: "IT",
            title: this.dataEditService.languages[0].title,
            description: this.dataEditService.languages[0].description
          },
          {
            language: "EN",
            title: this.dataEditService.languages[1].title,
            description: this.dataEditService.languages[1].description
          }
        ]
      }

      this.serviziService.updateService(nuovoServizio)
        .subscribe(resp => {
          let language = window.navigator.language;
          language = language.substring(0, 2);
          if (language === 'it') {
           this.global.showSuccess(this.global.modServiceErrorit)
         }else if(language === 'en'){
           this.global.showSuccess(this.global.modServiceErroren)
         }
          console.log('hai modificato il servizio: ' + resp);
          const dialogRef = this.dialog.closeAll()
          this.evetEm.entry.emit('ok');
        })
    }
  }

  downloadPDF(pdf) {
    const linkSource = `data:application/pdf;base64,${pdf}`;
    const downloadLink = document.createElement("a");
    const fileName = "vct_illustration.pdf";

    downloadLink.href = linkSource;
    downloadLink.download = fileName;
    downloadLink.click();
  }
}
