import { EmitService } from './../../services/emit.service';
import { TranslateService } from '@ngx-translate/core';
import { UpPwdService } from './../../services/up-pwd.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalService } from 'app/shared/global.service';

import * as $ from 'jquery';
import { NewPwd } from 'app/interfaces/new-pwd';
import { ServiziService } from 'app/services/servizi.service';
import IProviderInfoDTO from 'app/interfaces/IProviderInfoDTO';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { SignupService } from 'app/services/signup.service';
declare interface TableData {
  headerRow: string[];
  dataRows: string[][];
}

@Component({
  selector: 'account-cmp',
  moduleId: module.id,
  templateUrl: 'account.component.html'
})

export class AccountComponent implements OnInit {


  email: string;
  oldpwd: string;
  newpwd: string;

  localEmail: any = localStorage.getItem('email');

  providerDetails: IProviderInfoDTO = {
    providerid: '',
    general: {
      companyname: '',
      legalstatusid: 1,
      businessareaid: 1,
      sectorid: 1,
      categoryid: 1,
      idi: '',
      iban: ''
    },
    addresses: [
      {
        addresstype: 1,
        address: '',
        nap: '',
        city: '',
        district: ''
      }
    ],
    referrers: [
      {
        name: '',
        lastname: '',
        email: '',
        department: '',
        phone: '',
        mobile: ''
      }
    ]
  }

  legal_status: any;
  businessarea: any;
  sectors: any;
  category: any;

  constructor(
    private global: GlobalService,
    private translateService: TranslateService,
    private evtEmitter: EmitService,
    private uppwd: UpPwdService,
    private router: Router,
    private servizi: ServiziService,
    private signupService: SignupService) {
    this.getProviderDetails()
  }



  ngOnInit() {
    this.translateService.setDefaultLang('it');
    this.translateService.use(localStorage.getItem('lang'));

    this.getLegalStatus();
    this.getBusinessArea();
    this.getSectors();
    this.getCategory();
    this.evtEmitter.entry.subscribe(resp => {
      this.ngOnInit();
    })

  }


  changePassword() {

    let newPwd: NewPwd = {
      email: this.email,
      oldpwd: this.oldpwd,
      newpwd: this.newpwd
    }

    if (newPwd.email != '' && newPwd.oldpwd != '' && newPwd.newpwd != '') {
      this.uppwd.updatePwd(newPwd).subscribe(resp => {
        let language = window.navigator.language;
        language = language.substring(0, 2);
        if(language === 'it'){
          this.global.showSuccess(this.global.modpwdSuccit)
        }else if(language === 'en'){
          this.global.showSuccess(this.global.modpwdSuccen)
        }
      })
    }else{
      let language = window.navigator.language;
      language = language.substring(0, 2);
      if(language === 'it'){
        this.global.showError(this.global.addServiceErrorit)
      }else if(language === 'en'){
        this.global.showError(this.global.addServiceErroren)
      }
    }


  }




  getProviderDetails() {
    this.servizi.getProviderDetails().subscribe((provider: any) => {



      this.providerDetails = provider

      console.log('provider', this.providerDetails);

    }, err => {
      this.global.showError('Si è verificato un errore')
    })
  }

  getLegalStatus() {
    this.signupService.getLegalStatus()
      .subscribe(resp => {
        this.legal_status = resp;
      })
  }

  getBusinessArea() {
    this.signupService.getBusinessArea()
      .subscribe(resp => {
        this.businessarea = resp;
      })
  }

  getSectors() {
    this.signupService.getSectors()
      .subscribe(resp => {
        this.sectors = resp;
      })
  }

  getCategory() {
    this.signupService.getCategory()
      .subscribe(resp => {
        this.category = resp;
      })
  }

}
