import { EmitService } from './../../services/emit.service';
import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../shared/global.service';
import { ActivatedRoute, Router } from '@angular/router';
import * as $ from 'jquery';
import { ConvenzioniService } from 'app/services/convenzioni.service';


declare interface TableData {
  headerRow: string[];
  dataRows: string[][];
}

@Component({
  selector: 'add-edit-convenzione-cmp',
  moduleId: module.id,
  templateUrl: 'add-edit-convenzione.component.html'
}) 

export class AddEditConvenzioneComponent implements OnInit {

  convenzione: any
  aziende: any = []

  constructor(

    private global: GlobalService,
    private convService: ConvenzioniService,
    private translateService: TranslateService,
    private router: Router,
    private evetEm: EmitService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.translateService.setDefaultLang('it');
    this.translateService.use(localStorage.getItem('lang'));
    //this.lang = localStorage.getItem('lang');
    this.evetEm.entry.subscribe(resp => {
      this.ngOnInit();
    })
    this.getAgreementNumber();
  }

  getAgreementNumber() {
    this.route.params.subscribe(params => {
      //console.log(params.id);
      this.convService.getAgreementNumber(params.id).subscribe(resp => {
        console.log(resp);
        this.convenzione = resp;
      })
    })
  }
}