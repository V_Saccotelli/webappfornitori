import { Router } from '@angular/router';
import { LoginService } from './../../services/login.service';
import { Component, OnInit } from '@angular/core';
import { GlobalService } from 'app/shared/global.service';
@Component({
  selector: 'app-cambiapwd',
  templateUrl: './cambiapwd.component.html',
  styleUrls: ['./cambiapwd.component.css']
})
export class CambiapwdComponent implements OnInit {

  email:string;
  oldpassword:string;
  newpassword:string;
  constructor(private login: LoginService, private router: Router,private global: GlobalService) { }

  ngOnInit(): void {
  }

  changeExpiredPassword(){
    if (this.email,this.oldpassword,this.newpassword){
    this.login.changeExpiredPassword(this.email,this.oldpassword,this.newpassword).subscribe(resp =>
      {
        console.log(resp);
        this.global.showSuccess('Password modificata correttamente')
      this.router.navigate(["homepage"]);
      }, error => {
        console.log('error: ', error);
      })
    }
  }

  showPassword(id) {
    console.log(id);
    
    var x: any = document.getElementById(id);
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }


}
