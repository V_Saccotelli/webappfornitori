import { TranslateService } from '@ngx-translate/core';
import { EmitService } from './../../services/emit.service';
import { FixedPluginComponent } from './../../shared/fixedplugin/fixedplugin.component';

import { DialogFormComponent } from '../dialog-form/dialog-form.component';

import { SectionService } from './../../services/section.service';
import { MediaService } from './../../services/media.service';
import { Imediadto } from './../../interfaces/imediadto';
import { ServiziService } from './../../services/servizi.service';
import { Component, OnInit, PipeTransform } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from 'app/shared/global.service';
import { serialize } from 'chartist';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';

/* prova tab con filtro */
import { DecimalPipe, NumberSymbol } from '@angular/common';
declare interface TableData {
  headerRow: string[];
  dataRows: string[][];
}

@Component({
  selector: 'categorieServizi-cmp',
  moduleId: module.id,
  templateUrl: 'categorieServizi.component.html',
  providers: [DecimalPipe]
})

export class categorieServiziComponent implements OnInit {

  service: any = { languages: [{}] };
  closeResult = '';
  file: any;
  loadedFile: any;
  loadedFileId: string;
  mediatype!: any;
  loadedVideo: any;
  image: any;
  video: any;
  loadedPicture: any;
  loadedPictureId: string;
  loadedPicId: any;
  loadedVideoId: any;
  id: any;
  uploadedFile: any;
  uploadedFileId: string;
  typologies: any;
  services: any;

  allTypologies: any;

  categorie: any = []
  servizi: any = []

  titleIT!: string;
  descrIT!: string;
  titleEN!: string;
  descrEN!: string;
  sezione!: string;
  prezzo!: number;
  sconto!: number;
  importo_sconto!: number;
  sconto_medio: number;
  typologyid!: any;

  servicesStatusUno: any;
  searchForTypology: any;

  rispostaServiceid: any;

  arrServiziTipologia: any;
  constructor(
    public pipe: DecimalPipe,
    private translateService: TranslateService,
    private modalService: NgbModal,
    private mediaService: MediaService,
    private serviziService: ServiziService,
    private global: GlobalService,
    private sectionService: SectionService,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private evetEm: EmitService,
    private globalService: GlobalService,
    private router: Router) { }



  ngOnInit() {
    this.translateService.setDefaultLang('it');
    this.translateService.use(localStorage.getItem('lang'));
    this.evetEm.entry.subscribe(resp => {
      this.ngOnInit();
    })

    //window.location.reload();
    this.getFilteredServicesStatusUno(null);
    this.getFilteredServicesStatusDue(null);
    this.getTypologies();
    this.evetEm.entry.subscribe(resp => {
      //console.log(resp);
      this.getFilteredServicesStatusUno(null);
      this.getFilteredServicesStatusDue(null);
    })
  }

  getAllTypologies() {
    this.serviziService.getAllTypologies()
      .subscribe(resp => {
        //console.log(resp);
        this.allTypologies = resp;
      })
  }

  chooseTypology(evt) {

    this.getFilteredServicesStatusUno(evt)
    this.getFilteredServicesStatusDue(evt)


    //this.evetEm.entry.emit('ok');
    //console.log(evt);
   // const evtTypo = evt;
    //this.servicesStatusUno = this.servicesStatusUno.filter(item => item.typology.id === evtTypo);
    //this.services = this.services.filter((item => item.typology.id === evtTypo))


  }

  resetFilterTypology() {
    this.evetEm.entry.emit('ok');
  }

  searchServices(evt) {
    //console.log(evt);
    const search = evt.target.value;
    if (search && search.trim() != '') {
      this.servicesStatusUno = this.servicesStatusUno.filter((item) => {
        if (item.servicestitle.toLowerCase().indexOf(search.toLowerCase()) > -1) {
          return true;
        } else {
          return false;
        }
      });
    } else {
      this.getFilteredServicesStatusUno(null);
    }
  }

  searchServicesNoActivated(evt) {
    const search = evt.target.value;
    if (search && search.trim() != '') {
      this.services = this.services.filter((item) => {
        if (item.servicestitle.toLowerCase().indexOf(search.toLowerCase()) > -1) {
          return true;
        } else {
          return false;
        }
      });
    } else {
      this.getFilteredServicesStatusDue(null);
    }

  }
  getFilteredServicesStatusUno(filter) {
    var data: any = {
      language: "IT",
      title: "",
      providerid: localStorage.getItem('providerID'),
      status: 1,
      isanonymous: 0
    }
    this.serviziService.getFilteredServices(data).subscribe((res: any) => {
      const providers = res.response
      //console.log(providers)

      let serviceGrouped = []
      for (let singleProvider of providers) {
        serviceGrouped = [...serviceGrouped, ...singleProvider.services]
      }
      this.servicesStatusUno = serviceGrouped
      //console.log("serviceGrouped", serviceGrouped)

      if(filter){
        console.log('filtro',filter);
        this.servicesStatusUno = this.servicesStatusUno.filter(item => item.typology.id === filter);
      }

      //console.log(serviceGrouped[0].serviceid)
    });
  }

  resetFilter(){
    this.getFilteredServicesStatusUno(null);
    this.getFilteredServicesStatusDue(null);
  }
  getFilteredServicesStatusDue(filter) {
    var data: any = {
      language: "IT",
      title: "",
      providerid: localStorage.getItem('providerID'),
      status: 2,
      isanonymous: 0
    }
    this.serviziService.getFilteredServices(data).subscribe((res: any) => {
      const providers = res.response
      //console.log(providers)

      let serviceGrouped = []
      for (let singleProvider of providers) {
        serviceGrouped = [...serviceGrouped, ...singleProvider.services]
      }
      this.services = serviceGrouped
      //console.log("serviceGrouped", serviceGrouped )
      //console.log(serviceGrouped[0].serviceid)
      if(filter){
        console.log('filtro',filter);
        this.services = this.services.filter(item => item.typology.id === filter);
      }
    });
  }
  getTypologies() {
    this.serviziService.getAllTypologies()
      .subscribe(resp => {
        //console.log(resp);
        this.allTypologies = resp;
      })
  }
  getAllTypologiesByLanguage(language) {
    const call = this.sectionService.getAllTypologiesByLanguage(language).subscribe((data: any) => {
      this.typologies = data;
    })
    return call
  }
  getServiceDetails() {
    this.serviziService
      .getServiceDataById(this.id)
      .subscribe((data: any) => {
        console.log(data, "SERVIZIO SINGOLO")
        this.service = data
        this.mediatype = data.mediatype
        this.service.languageItTitle = this.getLanguages(this.service.languages, "IT")
        this.service.languageEnTitle = this.getLanguages(this.service.languages, "EN")
        this.service.languageItDesc = this.getLanguages(this.service.languages, "IT")
        this.service.languageEnDesc = this.getLanguages(this.service.languages, "EN")
        if (this.service.mediatype == 1 && this.service.mediaid != null) {
          this.getPictureById(this.service.mediaid)
        } else if (this.service.mediatype == 2 && this.service.mediaid != null) {
          this.getVideoById(this.service.mediaid)
        }
        if (this.service.documentid != "" && this.service.documentid != " ") {
          this.getDocFileById(this.service.documentid)
        }
      })
  }
  getLanguages(languages: any[], selectedLanguage = "IT") {
    return languages.find(
      (singleLanguage: any) => singleLanguage.language === selectedLanguage
    )?.title;
  }
  onDocChange(event) {
    var that = this;
    this.file = event.target.files;
    var reader = new FileReader();
    reader.readAsDataURL(this.file[0]);
    reader.onload = function () {
      var base64: any = reader.result;
      base64 = base64.substring(base64.lastIndexOf(",") + 1);
      var file: Imediadto = {
        mediaobject: base64,
      };
      that.addMediaDocument(file);
    };
    reader.onerror = function (error) {
      console.error(error)
    };
  }
  addMediaDocument(file: Imediadto) {
    this.mediaService.addMediaDocumentOrFile(file).subscribe((fileResult: any) => {
      this.loadedFile = file.mediaobject
      this.loadedFileId = fileResult.id

      console.log('loadedFile', this.loadedFile);
      console.log('loadedFileId', this.loadedFileId);
    }, err => {
      this.globalService.showError("Il file è troppo grande" + err);
    })
  }
  onImageChange(event) {
    var that = this;
    this.image = event.target.files;
    // console.log("image", this.image);
    var reader = new FileReader();
    reader.readAsDataURL(this.image[0]);
    reader.onload = function () {
      var base64: any = reader.result;
      base64 = base64.substring(base64.lastIndexOf(",") + 1);
      var pic: Imediadto = {
        mediaobject: base64,
      };
      that.addMediaPicture(pic);
    };
    reader.onerror = function (error) { };
  }
  onVideoChange(event) {
    var that = this;
    this.video = event.target.files;
    var reader = new FileReader();
    reader.readAsDataURL(this.video[0]);
    reader.onload = function () {
      var base64: any = reader.result;
      base64 = base64.substring(base64.lastIndexOf(",") + 1);
      var video: Imediadto = {
        mediaobject: base64,
      };
      that.addMediaMovie(video);
    };
    reader.onerror = function (error) { };
  }
  addMediaPicture(pic: Imediadto) {
    this.mediaService.addMediaPicture(pic).subscribe((picResult: any) => {
      // console.log("pic id", picResult)
      this.loadedPicture = pic.mediaobject
      this.loadedPicId = picResult.id
      // console.log("picture", this.loadedPicture)
    }, err => {
      this.globalService.showError("Il file è troppo grande")
    }

    )
  }
  addMediaMovie(video: Imediadto) {
    this.mediaService.addMediaMovie(video).subscribe((videoResult: any) => {
      console.log(" video id", videoResult)
      this.loadedVideo = video.mediaobject
      this.loadedVideoId = videoResult.id
      console.log("video", this.loadedVideo)
    }, err => {
      this.globalService.showError("Il file è troppo grande")
    })
  }

  getPictureById(pictureid: string) {
    this.mediaService.getPictureFileById(pictureid).subscribe((pictureResult: any) => {
      // console.log("picResult", pictureResult)
      this.loadedPicture = pictureResult.image
      this.loadedPictureId = pictureid
      // console.log("picResult", this.loadedPicture, this.loadedPictureId)
    })
  }

  getVideoById(videoid: string) {
    this.mediaService.getVideoFileById(videoid).subscribe((videoResult: any) => {
      console.log("[getVideoById]videoResult", videoResult)
      this.loadedVideo = videoResult.video
      this.loadedVideoId = videoid
    })
  }
  getDocFileById(fileid: string) {
    // console.log(fileid, "file")
    this.mediaService.getDocFileById(fileid).subscribe((fileResult: any) => {
      console.log("fileResult", fileResult)

      this.uploadedFile = fileResult.file
      this.uploadedFileId = fileid
      console.log(this.uploadedFile, "file loaded")
    })
  }



  openDialog() {
    const dialogRef = this.dialog.open(DialogFormComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
  deleteService(service: any) {
    var result = confirm('Sicuro di voler eliminare il servizio?');
    if (result == true) {
      this.serviziService.deleteService(service.serviceid).subscribe(resp => {
        console.log(resp);
        alert('Servizio Eliminato Correttamente');
        this.evetEm.entry.emit('ok');
      })
    } else {
      alert('Servizio Non Eliminato');
    }
  }
  updateService(serviceEdit: any) {
    //this.router.navigate(['updateservice', service.serviceid]);
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = serviceEdit;
    const dialogRef = this.dialog.open(DialogFormComponent, dialogConfig);
    dialogRef.afterOpened().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      //console.log(serviceEdit);
    });
  }
  onMediaChange(event) {
    this.mediatype = event
    console.log(this.mediatype, "mediatype")
  }
}

