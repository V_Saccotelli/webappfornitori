import { EmitService } from './../../services/emit.service';
import { TranslateService } from '@ngx-translate/core';
import { SignupService } from "./../../services/signup.service";
import { Component, OnInit } from "@angular/core";
import { Signup } from "app/interfaces/signup";
import { GlobalService } from "app/shared/global.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-signup",
  templateUrl: "./signup.component.html",
  styleUrls: ["./signup.component.css"],
})
export class SignupComponent implements OnInit {
  ragione_sociale: string;
  legal_status_id: any;
  businessarea: any;
  sectors: any;
  category: any;
  idi: string;
  iban: string;
  address: string;
  nap: number;
  city: string;
  district: string;
  name: string;
  lastname: string;
  email: string;
  department: string;
  phone: number;
  mobile: number;




  /* ID select */
  legalStatusId: any = 1;
  businessarea_id: any = 1;
  sector_id: any = 1;
  category_id: any = 1;
  constructor(private signupService: SignupService,
    private global: GlobalService,
    private router: Router,
    private translateService: TranslateService,
    private evtEmitter: EmitService) { }

  ngOnInit(): void {
    this.translateService.setDefaultLang('it');
    this.translateService.use(localStorage.getItem('lang'));
    //this.lang = localStorage.getItem('lang');
    this.evtEmitter.entry.subscribe(resp => {
      this.ngOnInit();
    })
    this.getLegalStatus();
    this.getBusinessArea();
    this.getSectors();
    this.getCategory();
  }
  getLegalStatus() {
    this.signupService.getLegalStatus()
      .subscribe(resp => {
        this.legal_status_id = resp;
      })
  }

  getBusinessArea() {
    this.signupService.getBusinessArea()
      .subscribe(resp => {
        this.businessarea = resp;
      })
  }

  getSectors() {
    this.signupService.getSectors()
      .subscribe(resp => {
        this.sectors = resp;
      })
  }

  getCategory() {
    this.signupService.getCategory()
      .subscribe(resp => {
        this.category = resp;
      })
  }

  chooseLegalStatus(evt) {
    console.log(evt);
  }
  chooseBusinessArea(evt) {
    console.log(evt);
  }
  chooseSectors(evt) {
    console.log(evt);
  }
  chooseCategory(evt) {
    console.log(evt)
  }

  createProvider() {



    let objProvider: Signup = {
      providergeneral: {
        companyname: this.ragione_sociale,
        legalstatusid: this.legalStatusId,
        businessareaid: this.businessarea_id,
        sectorid: this.sector_id,
        categoryid: this.category_id,
        IDI: this.idi,
        IBAN: this.iban,
      },
      provideraddress: {
        address: this.address,
        NAP: this.nap,
        city: this.city,
        district: this.district,
      },
      providerreferrer: {
        name: this.name,
        lastname: this.lastname,
        email: this.email,
        department: this.department,
        phone: this.phone,
        mobile: this.mobile,
      },
    };
    this.signupService.signup(objProvider).subscribe((resp) => {
      let language = window.navigator.language;
      language = language.substring(0, 2);
      if (language === 'it') {
        this.global.showSuccess(this.global.successit1)
      }else if(language === 'en'){
        this.global.showSuccess(this.global.successen1)
      }

      this.router.navigate(['/homepage']);
    }, err => {
      let language = window.navigator.language;
      language = language.substring(0, 2);
      if(language === 'it'){
        this.global.showError(this.global.errorit1)
      }else if(language === 'en'){
        this.global.showError(this.global.erroren1)
      }
    });
  }
}
