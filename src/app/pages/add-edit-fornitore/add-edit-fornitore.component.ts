import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../shared/global.service';
import { Router } from '@angular/router';
import * as $ from 'jquery';


declare interface TableData {
    headerRow: string[];
    dataRows: string[][];
}

@Component({
    selector: 'add-edit-fornitore-cmp',
    moduleId: module.id,
    templateUrl: 'add-edit-fornitore.component.html'
})

export class AddEdiFornitoreComponent implements OnInit {

    fornitore:any

    constructor(
        
        private global: GlobalService,
        private router: Router) {

      
            this.fornitore = this.router.getCurrentNavigation().extras.queryParams.fornitore
            console.log('fornitore',this.router.getCurrentNavigation());

    }

    ngOnInit() {

      
    }


}
