import { HttpClient } from '@angular/common/http';
import { EmitService } from './../services/emit.service';
import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit } from '@angular/core';




export interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
  toshow: boolean;
}

export const ROUTES: RouteInfo[] = [
  { path: '/convenzioni', title: 'Convenzioni', icon: 'nc-briefcase-24', class: 'nc-badge', toshow: true },
  { path: '/categorieservizi', title: 'CategorieServizi', icon: 'nc-tile-56', class: '', toshow: true },
  { path: '/report', title: 'Report', icon: 'nc-chart-pie-36', class: '', toshow: true },
  { path: '/gestioneaccount', title: 'GestioneAccount', icon: 'nc-settings-gear-65', class: '', toshow: true },
  // { path: '/dashboard',     title: 'Dashboard',         icon:'nc-bank',       class: '' },
  // { path: '/icons', title: 'Icons', icon: 'nc-diamond', class: '', toshow: false },
  // { path: '/maps',          title: 'Maps',              icon:'nc-pin-3',      class: '' },
  // { path: '/notifications', title: 'Notifications',     icon:'nc-bell-55',    class: '' },

  // { path: '/table',         title: 'Table List',        icon:'nc-tile-56',    class: '' },
  // { path: '/typography',    title: 'Typography',        icon:'nc-caps-small', class: '' },
  // { path: '/upgrade',       title: 'Upgrade to PRO',    icon:'nc-spaceship',  class: 'active-pro' },
];



@Component({
  moduleId: module.id,
  selector: 'sidebar-cmp',
  templateUrl: 'sidebar.component.html',
})

export class SidebarComponent implements OnInit {
  public menuItems: any[];

  it
  en

  constructor(private translateService: TranslateService,
    private evetEm: EmitService, private http: HttpClient) { }


  ngOnInit() {

    this.http.get('assets/i18n/it.json').subscribe((it) => {
      this.it = it

    });

    this.http.get('assets/i18n/en.json').subscribe((en) => {
      this.en = en
    });


    this.menuItems = ROUTES.filter(menuItem => menuItem);

    this.translateService.setDefaultLang('it');
    this.translateService.use(localStorage.getItem('lang'));
    this.evetEm.entry.subscribe(resp => {
      this.ngOnInit();
    });

  }

  getTranslation(stri: string) {
    var value

    var lang = localStorage.getItem('lang')
    this.translateService.setDefaultLang(lang)
    const currentLang = this.translateService.getDefaultLang(); // get current language

    var myjsonlang
    if(currentLang === 'it'){
      myjsonlang = this.it
    }else{
      myjsonlang = this.en
    }
    Object.keys(myjsonlang).forEach(function (stringa) {
      if(stringa === stri){
        value = myjsonlang[stringa];
        return value
      }
    return value
    });
    return value
  }
}
