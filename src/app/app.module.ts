
import { InterceptorLanguage } from './interceptor/interceptor-language';

import { MyHttpInterceptor } from './interceptor/my-http-interceptor';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ToastrModule } from "ngx-toastr";
import { SidebarModule } from './sidebar/sidebar.module';
import { FooterModule } from './shared/footer/footer.module';
import { NavbarModule } from './shared/navbar/navbar.module';
import { FixedPluginModule } from './shared/fixedplugin/fixedplugin.module';
import {MatMenuModule} from '@angular/material/menu';
import { AppComponent } from './app.component';
import { AppRoutes } from './app.routing';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { AuthGuardService } from './auth/auth-guard.service';
import { GlobalService } from './shared/global.service';
import { AuthService } from './auth/auth.service';
import { JwtModule, JwtHelperService, JWT_OPTIONS } from '@auth0/angular-jwt';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LandingComponent } from './pages/landing/landing.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { SignupComponent } from './pages/signup/signup.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CambiapwdComponent } from './pages/cambiapwd/cambiapwd.component';
import { UpdateserviceComponent } from './pages/updateservice/updateservice.component';
import {MatDialogModule} from '@angular/material/dialog';
import { DialogFormComponent } from './pages/dialog-form/dialog-form.component';



export function HttpLoaderFactory(http: HttpClient){
  return new TranslateHttpLoader(http);
}
MyHttpInterceptor
@NgModule({
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    LandingComponent,
    SignupComponent,
    CambiapwdComponent,
    UpdateserviceComponent,
    DialogFormComponent
  ],
  imports: [
    BrowserAnimationsModule,
    JwtModule,
    MatMenuModule,
    RouterModule.forRoot(AppRoutes, {
      useHash: true
    }),
    SidebarModule,
    NavbarModule,
    ToastrModule.forRoot({
      timeOut: 5000,
      closeButton: true,
      positionClass: 'toast-top-center',
      preventDuplicates: true,

    }),
    FormsModule,
    ReactiveFormsModule,
    FooterModule,
    FixedPluginModule,
    HttpClientModule,
    MatDialogModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
  })
  ],
  providers: [AuthGuardService,
    {provide: HTTP_INTERCEPTORS, useClass:MyHttpInterceptor, multi:true},
    {provide: HTTP_INTERCEPTORS, useClass: InterceptorLanguage, multi:true},
    GlobalService,
    AuthService,
    JwtHelperService,
    TranslateModule,
    HttpClient,
    { provide: JWT_OPTIONS, useValue: JWT_OPTIONS },],
  bootstrap: [AppComponent]
})
export class AppModule { }
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
