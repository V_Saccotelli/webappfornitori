import { HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatNativeDateModule} from '@angular/material/core';


import { AdminLayoutRoutes } from './admin-layout.routing';

import { DashboardComponent } from '../../pages/dashboard/dashboard.component';

import { IconsComponent } from '../../pages/icons/icons.component';
import { MapsComponent } from '../../pages/maps/maps.component';
import { NotificationsComponent } from '../../pages/notifications/notifications.component';


import { ConvenzioniComponent } from '../../pages/convenzioni/convenzioni.component';

import { categorieServiziComponent } from '../../pages/categorieServizi/categorieServizi.component';
import { AccountComponent } from '../../pages/account/account.component';
import {HttpClientModule} from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { JwtHelperService, JWT_OPTIONS } from '@auth0/angular-jwt';

import { MatTabsModule } from '@angular/material/tabs';
import { MatInputModule } from '@angular/material/input';
import { AddEditAdminComponent } from '../../pages/add-edit-admin/add-edit-admin.component';
import {MatMenuModule} from '@angular/material/menu';
import { AddEditAziendaComponent } from 'app/pages/add-edit-azienda/add-edit-azienda.component';
import { AddEdiFornitoreComponent } from 'app/pages/add-edit-fornitore/add-edit-fornitore.component';
import { AddEditConvenzioneComponent } from 'app/pages/add-edit-convenzione/add-edit-convenzione.component';
import { ReportComponent } from 'app/pages/report/report.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HttpLoaderFactory } from 'app/app.module';
import {MatButtonModule} from '@angular/material/button';



@NgModule({
  imports: [
    CommonModule,
    MatMenuModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    NgbModule,
    MatTabsModule,
    MatInputModule,
    ReactiveFormsModule,
    MatButtonModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
  })

  ],
  declarations: [
    DashboardComponent,
    IconsComponent,
    MapsComponent,
    NotificationsComponent,
    ConvenzioniComponent,
    categorieServiziComponent,
    AccountComponent,
    AddEditAdminComponent,
    AddEditAziendaComponent,
    AddEdiFornitoreComponent,
    AddEditConvenzioneComponent,
    ReportComponent
  ],
  providers: [JwtHelperService]
})

export class AdminLayoutModule { }
