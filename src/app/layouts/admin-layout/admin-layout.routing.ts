import { Routes } from '@angular/router';

import { DashboardComponent } from '../../pages/dashboard/dashboard.component';

import { IconsComponent } from '../../pages/icons/icons.component';
import { MapsComponent } from '../../pages/maps/maps.component';
import { NotificationsComponent } from '../../pages/notifications/notifications.component';
import { categorieServiziComponent } from '../../pages/categorieServizi/categorieServizi.component';
import { AccountComponent } from '../../pages/account/account.component';
import { AddEditAdminComponent } from 'app/pages/add-edit-admin/add-edit-admin.component';
import { AddEditAziendaComponent } from 'app/pages/add-edit-azienda/add-edit-azienda.component';
import { AddEdiFornitoreComponent } from 'app/pages/add-edit-fornitore/add-edit-fornitore.component';
import { AddEditConvenzioneComponent } from 'app/pages/add-edit-convenzione/add-edit-convenzione.component';
import { ConvenzioniComponent } from 'app/pages/convenzioni/convenzioni.component';
import { ReportComponent } from 'app/pages/report/report.component';



export const AdminLayoutRoutes: Routes = [
    // { path: 'dashboard',      component: DashboardComponent },

    // { path: 'table',          component: TableComponent },
    // { path: 'typography',     component: TypographyComponent },
    // { path: 'icons',          component: IconsComponent },
    // { path: 'maps',           component: MapsComponent },
    // { path: 'notifications',  component: NotificationsComponent },
    // { path: 'upgrade',        component: UpgradeComponent },
    { path: 'convenzioni', component: ConvenzioniComponent },
    { path: 'categorieservizi', component: categorieServiziComponent },
    { path: 'gestioneaccount', component: AccountComponent },
    { path: 'admin', component: AddEditAdminComponent },
    { path: 'azienda', component: AddEditAziendaComponent },
    { path: 'fornitore', component: AddEdiFornitoreComponent },
    { path: 'convenzione/:id', component: AddEditConvenzioneComponent },
    { path: 'report', component: ReportComponent }
];
