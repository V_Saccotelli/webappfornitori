
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { SignupComponent } from './pages/signup/signup.component';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { LandingComponent } from './pages/landing/landing.component';
import { Routes, CanActivate, RouterModule } from '@angular/router';
import { AuthGuardService as AuthGuard } from './auth/auth-guard.service';
import { CambiapwdComponent } from './pages/cambiapwd/cambiapwd.component';
import { DialogFormComponent } from './pages/dialog-form/dialog-form.component';
export const AppRoutes: Routes = [
  {
    path: '',
    redirectTo: 'homepage',
    pathMatch: 'full',
  }, {
    path: '',
    component: AdminLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: './layouts/admin-layout/admin-layout.module#AdminLayoutModule',
        canActivate: [AuthGuard]
      }]
  },
  {
    path: 'homepage',
    component: LandingComponent,
  },
  {
    path: '**',
    redirectTo: 'admins'
  },
  {
    path: 'signup', component: SignupComponent
  },
  {
    path: 'changepwd', component: CambiapwdComponent
  },
  {
    path: 'dashboard', component: DashboardComponent
  },

  {
    path: 'updateservice/:service', component: DialogFormComponent
  }
]
