import { GlobalService } from 'app/shared/global.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ServiziService {

  constructor(private http: HttpClient,  private global: GlobalService) { }

  createService(servizio: any){
         return this.http.post(environment.apiUrl+'service/create', servizio, { headers: this.global.getHeader() });
  }

  updateService(servizioModificato:any){
      return this.http.post(environment.apiUrl+'service/update', servizioModificato, { headers: this.global.getHeader() });
  }

  deleteService(serviceid:number){
     return this.http.delete(environment.apiUrl+'service/delete?serviceid='+serviceid, { headers: this.global.getHeader() });
  }
  getFilteredServices(data: any){
    let URL = environment.apiUrl + "service/get/filtered";
     let postdata = JSON.stringify(data)
     return this.http.post(URL, postdata, { headers: this.global.getHeader() });
   }

   getServiceDataById(serviceid: string){
    let URL = environment.apiUrl + "service/get?serviceid=" +serviceid;
     return this.http.get(URL, { headers: this.global.getHeader() });
   }

   getAllTypologies(){
    let URL = environment.apiUrl + "section/typology/get/filtered/list?language=IT";
     return this.http.get(URL, { headers: this.global.getHeader() });
   }

   getProviderDetails(){
    var providerid = localStorage.getItem('providerID')
    let URL = environment.apiUrl + `provider/get/${providerid}`;
    return this.http.get(URL, { headers: this.global.getHeader() });
   }
}
