import { environment } from './../../environments/environment';
import { GlobalService } from './../shared/global.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConvenzioniService {

  constructor(private http: HttpClient, private global: GlobalService) { }

  getAgreementDetail(){
    return this.http.get(environment.apiUrl+'agreement/provider/get/my?providerid='+ localStorage.getItem('providerID'), { headers: this.global.getHeader() });
  }

  getAgreementNumber(agrNumber: any){
    return this.http.get(environment.apiUrl+'agreement/provider/get/agreement?agreementnumber='+agrNumber);
  }

  unsubscribeAgreement(){
    return this.http.get(environment.apiUrl+'agreement/provider/unsubscribe?providerid='+ localStorage.getItem('providerID'), { headers: this.global.getHeader() })
  }
}
