import { GlobalService } from "app/shared/global.service";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "environments/environment";

@Injectable({
  providedIn: "root",
})
export class LoginService {
  //api/account/login
  constructor(private http: HttpClient, private global: GlobalService) {}

  login(email: string, password: string) {
      let postdata = JSON.stringify({
        account: email,
        password: password
      });
    return this.http.post('https://devdts.nextre.it/aiti/services/api/account/login', postdata, { headers: this.global.getHeader() });
  }

  changeExpiredPassword(email: string, oldpwd: string, newpwd: string) {
    let URL = environment.apiUrl + "account/provider/password/expired";
    let postdata = JSON.stringify({
      email: email,
      oldpwd: oldpwd,
      newpwd: newpwd,
    });
    return this.http.post(URL, postdata, { headers: this.global.getHeader() });
  }
}
