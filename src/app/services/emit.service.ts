import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmitService {

  public entry : EventEmitter<any> = new EventEmitter<any>();
  constructor() { }
}
