import { Imediadto } from './../interfaces/imediadto';
import { GlobalService } from './../shared/global.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MediaService {

  constructor(private http: HttpClient, private global: GlobalService) { }

  addMediaIcon(data: Imediadto){
   let URL = environment.apiUrl + "media/add/image/icon";
    let postdata = JSON.stringify(data)
    console.log('postdata',postdata);
    return this.http.post(URL, postdata, { headers: this.global.getHeader() });
  }

  addMediaMovie(data: Imediadto){
   let URL = environment.apiUrl + "media/add/movie";
    let postdata = JSON.stringify(data)
    return this.http.post(URL, postdata, { headers: this.global.getHeader() });
  }

  addMediaPicture(data: Imediadto){
   let URL = environment.apiUrl + "media/add/image/jpgpng";
    let postdata = JSON.stringify(data)
    return this.http.post(URL, postdata, { headers: this.global.getHeader() });
  }
  addMediaDocumentOrFile(data: Imediadto){
   let URL = environment.apiUrl + "media/add/document";
    let postdata = JSON.stringify(data)
    return this.http.post(URL, postdata, { headers: this.global.getHeader() });
  }
  getIconFileByIconId(iconid){
    let URL = environment.apiUrl + "media/get/icon";
    return this.http.get(URL, { headers: this.global.getHeader(), params: { iconid } });
  }
  getPictureFileById(pictureid){
    let URL = environment.apiUrl + "media/get/picture";
    return this.http.get(URL, { headers: this.global.getHeader(), params: { pictureid } });
  }
  getVideoFileById(videoid){
    let URL = environment.apiUrl + "media/get/video";
    return this.http.get(URL, { headers: this.global.getHeader(), params: { videoid } });
  }
  getDocFileById(fileid){
    let URL = environment.apiUrl + "media/get/file";
    return this.http.get(URL, { headers: this.global.getHeader(), params: { fileid } });
  }
}

