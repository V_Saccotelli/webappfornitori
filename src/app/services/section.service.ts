import { environment } from 'environments/environment';
import { GlobalService } from 'app/shared/global.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SectionService {

  constructor(private http: HttpClient, private global: GlobalService) { }

  getAllTypologiesByLanguage(language: string){
    let URL = environment.apiUrl + "section/typology/get/filtered/list?language=" + language ;

    return this.http.get(URL, { headers: this.global.getHeader()});
  }
}
