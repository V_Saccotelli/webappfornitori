import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NewPwd } from 'app/interfaces/new-pwd';
import { environment } from 'environments/environment';
@Injectable({
  providedIn: 'root'
})
export class UpPwdService {

  constructor(private http: HttpClient) { }

  updatePwd(newPwd: NewPwd){
       return this.http.post(environment.apiUrl+'account/provider/password/change', newPwd);
  }
}
