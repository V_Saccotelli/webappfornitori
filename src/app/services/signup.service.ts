import { GlobalService } from './../shared/global.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SignupService {

  constructor(private http: HttpClient, private global: GlobalService) { }

  signup(provider:any){
    let postProvider = JSON.stringify(provider);
    //console.log("postProvider", postProvider);
    return this.http.post(environment.apiUrl+'provider/self/register', postProvider,  { headers: this.global.getHeader()});
  }

  getLegalStatus(){
    return this.http.get(environment.apiUrl+'configuration/get/legalstatus');
  }

  getBusinessArea(){
    return this.http.get(environment.apiUrl+'configuration/get/businessarea');
  }

  getSectors(){
    return this.http.get(environment.apiUrl+'configuration/get/sectors');
  }

  getCategory(){
    return this.http.get(environment.apiUrl+'configuration/get/categories');
  }
}
